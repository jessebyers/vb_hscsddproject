VERSION 5.00
Begin VB.Form frmSchoolEmail 
   BackColor       =   &H0080C0FF&
   Caption         =   "School Email"
   ClientHeight    =   5325
   ClientLeft      =   4260
   ClientTop       =   4485
   ClientWidth     =   10260
   ForeColor       =   &H00000000&
   LinkTopic       =   "Form1"
   ScaleHeight     =   5325
   ScaleWidth      =   10260
   Begin VB.CommandButton cmdSort 
      Caption         =   "Sort Alphabetically"
      Height          =   495
      Left            =   4440
      TabIndex        =   4
      Top             =   4560
      Width           =   2295
   End
   Begin VB.CommandButton cmdSearch 
      Caption         =   "Search"
      Height          =   495
      Left            =   840
      TabIndex        =   3
      Top             =   2280
      Width           =   1935
   End
   Begin VB.TextBox txtSearch 
      Height          =   615
      Left            =   600
      TabIndex        =   2
      Top             =   1440
      Width           =   2415
   End
   Begin VB.ListBox lstAddress 
      Height          =   3375
      Left            =   4080
      TabIndex        =   1
      Top             =   960
      Width           =   3015
   End
   Begin VB.CommandButton cmdBack 
      Caption         =   "Back"
      Height          =   615
      Left            =   8760
      TabIndex        =   0
      Top             =   4560
      Width           =   1335
   End
   Begin VB.Label Label1 
      BackColor       =   &H0080C0FF&
      Caption         =   "School Email Addresses"
      BeginProperty Font 
         Name            =   "Microsoft Sans Serif"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3120
      TabIndex        =   5
      Top             =   120
      Width           =   4095
   End
   Begin VB.Image Image2 
      Height          =   1095
      Left            =   9000
      Picture         =   "frmStudentEmail.frx":0000
      Stretch         =   -1  'True
      Top             =   120
      Width           =   1095
   End
End
Attribute VB_Name = "frmSchoolEmail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdBack_Click()
frmMainMenu.Show
Unload Me
End Sub

