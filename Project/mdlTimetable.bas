Attribute VB_Name = "mdlTimetable"
Option Explicit

Global arrSubjects(0 To 1, 0 To 4) As String

Global PerArray() As Timetable
Global PerCount As Integer

Type Timetable
Period As String * 8
Subject As String * 10
Time As String * 14
ClassRoom As String * 3
End Type

Public Sub ReadTimeTableFile()
Dim TimeTableFile As String
Dim txtLine
On Error Resume Next
TimeTableFile = App.Path & "\TextFiles\txttimetable.txt"
Open TimeTableFile For Input As 1 'path
While Not EOF(1)

Line Input #1, txtLine
If txtLine <> "" Then
PerCount = PerCount + 1

ReDim Preserve PerArray(PerCount)
PerArray(PerCount).Period = Mid(txtLine, 1, 8) '1 to 8
PerArray(PerCount).Subject = Mid(txtLine, 10, 10) ' columns 10 - 20
PerArray(PerCount).Time = Mid(txtLine, 21, 13) 'columns 21 to 34
PerArray(PerCount).ClassRoom = Mid(txtLine, 35, 5) ' columns 34 to 38
'MsgBox PerCount
End If
Wend

Close #1
End Sub

