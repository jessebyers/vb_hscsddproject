VERSION 5.00
Begin VB.Form frmSchoolInfo 
   BackColor       =   &H8000000D&
   Caption         =   "School Information"
   ClientHeight    =   5325
   ClientLeft      =   4275
   ClientTop       =   4500
   ClientWidth     =   10185
   Icon            =   "frmSchoolInfo.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5325
   ScaleWidth      =   10185
   Begin VB.CommandButton cmdBack 
      Caption         =   "Back"
      Height          =   615
      Left            =   8640
      TabIndex        =   5
      Top             =   4560
      Width           =   1335
   End
   Begin VB.Label lblEmail 
      BackColor       =   &H8000000D&
      Caption         =   "Email: admin@maitlandsm.catholic.edu.au"
      BeginProperty Font 
         Name            =   "Calibri"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   4
      Top             =   3480
      Width           =   5655
   End
   Begin VB.Label lblFax 
      BackColor       =   &H8000000D&
      Caption         =   "Fax: 4934 2667"
      BeginProperty Font 
         Name            =   "Calibri"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   3
      Top             =   2880
      Width           =   2175
   End
   Begin VB.Label lblPhone 
      BackColor       =   &H8000000D&
      Caption         =   "Phone: (02) 4933 6177"
      BeginProperty Font 
         Name            =   "Calibri"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   2
      Top             =   2280
      Width           =   3375
   End
   Begin VB.Label lblAddress 
      BackColor       =   &H8000000D&
      Caption         =   "Address: 16 Grant Street MAITLAND"
      BeginProperty Font 
         Name            =   "Calibri"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   1
      Top             =   1680
      Width           =   4815
   End
   Begin VB.Label lblStMarys 
      BackColor       =   &H8000000D&
      Caption         =   "All Saints College, St Mary's Campus MAITLAND "
      BeginProperty Font 
         Name            =   "Britannic Bold"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   240
      TabIndex        =   0
      Top             =   360
      Width           =   6975
   End
   Begin VB.Image Image1 
      Height          =   2250
      Left            =   7800
      Picture         =   "frmSchoolInfo.frx":1BFD9
      Top             =   120
      Width           =   2250
   End
End
Attribute VB_Name = "frmSchoolInfo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdBack_Click()
PerCount = 0
frmMainMenu.Show
Unload Me
End Sub

