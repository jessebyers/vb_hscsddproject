VERSION 5.00
Begin VB.Form frmLogin 
   BackColor       =   &H8000000D&
   Caption         =   "Login"
   ClientHeight    =   5355
   ClientLeft      =   4275
   ClientTop       =   4500
   ClientWidth     =   10245
   Icon            =   "frmLogin.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5355
   ScaleWidth      =   10245
   Begin VB.TextBox txtUserName 
      Height          =   375
      Left            =   3480
      TabIndex        =   6
      Top             =   1920
      Width           =   3255
   End
   Begin VB.CommandButton cmdRegister 
      Caption         =   "Register New Account"
      Height          =   615
      Left            =   4320
      TabIndex        =   5
      Top             =   4560
      Width           =   1455
   End
   Begin VB.TextBox txtPassword 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      IMEMode         =   3  'DISABLE
      Left            =   3480
      PasswordChar    =   "*"
      TabIndex        =   4
      Top             =   2640
      Width           =   3255
   End
   Begin VB.CommandButton cmdLogin 
      Caption         =   "Login"
      Height          =   615
      Left            =   360
      TabIndex        =   3
      Top             =   4560
      Width           =   1455
   End
   Begin VB.CommandButton cmdEnd 
      Caption         =   "Quit"
      Height          =   615
      Left            =   8520
      TabIndex        =   2
      Top             =   4560
      Width           =   1455
   End
   Begin VB.Label Label2 
      BackColor       =   &H8000000D&
      Caption         =   "St.Marys, ASC College, Maitland"
      BeginProperty Font 
         Name            =   "Rockwell"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2040
      TabIndex        =   8
      Top             =   720
      Width           =   6015
   End
   Begin VB.Image Image2 
      Height          =   2175
      Left            =   7320
      Picture         =   "frmLogin.frx":1BFD9
      Stretch         =   -1  'True
      Top             =   1680
      Width           =   2295
   End
   Begin VB.Label Label1 
      BackColor       =   &H8000000D&
      Caption         =   "Welcome To The eDiary for "
      BeginProperty Font 
         Name            =   "Rockwell"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2520
      TabIndex        =   7
      Top             =   120
      Width           =   5055
   End
   Begin VB.Image Image1 
      Height          =   1095
      Left            =   9000
      Picture         =   "frmLogin.frx":1E4AA
      Stretch         =   -1  'True
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label LabelPass 
      BackColor       =   &H8000000D&
      Caption         =   "Password:"
      BeginProperty Font 
         Name            =   "Arial Narrow"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1800
      TabIndex        =   1
      Top             =   2640
      Width           =   1575
   End
   Begin VB.Label LabelUser 
      BackColor       =   &H8000000D&
      Caption         =   "Username:"
      BeginProperty Font 
         Name            =   "Arial Narrow"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1800
      TabIndex        =   0
      Top             =   1920
      Width           =   1575
   End
End
Attribute VB_Name = "frmLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdEnd_Click()
End
End Sub

Private Sub cmdLogin_Click()
Dim intcount As Integer
Dim strUserTry As String

Call ReadStudentFile ' reads student.txt file, also reads mdlLogin
For intcount = 1 To StudentTotalCount
If txtUserName.Text = Trim(ArrStudentRecords(intcount).Username) Then 'if username matches and
If txtPassword.Text = Trim(ArrStudentRecords(intcount).Password) Then 'if password matches then
Unload Me
frmMainMenu.Show 'main menu shows
Exit Sub
End If
End If
Next intcount
MsgBox "Incorrect Username and/or Password", vbExclamation, "Incorrect" 'message about incorrect password
End Sub

Private Sub cmdRegister_Click()
frmRegister.Show
Unload Me
End Sub

Private Sub Form_Load()
PerCount = 0
End Sub
