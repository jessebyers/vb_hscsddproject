Attribute VB_Name = "mdlLogin"
Type Student
Username As String * 7
Password As String * 10
End Type
Global ArrStudentRecords() As Student
Global StudentTotalCount As Integer

Public Sub ReadStudentFile()
Dim strOneLine As String ' each line stored in this variable
On Error GoTo OopsLabel ' error trapping
strOneLine = App.Path & "\TextFiles\student.txt"
Open strOneLine For Input As 1

While Not EOF(1)
Line Input #1, strOneLine
If strOneLine <> "" Then
StudentTotalCount = StudentTotalCount + 1 'number of records
ReDim Preserve ArrStudentRecords(StudentTotalCount) 're dimensions the size of array
ArrStudentRecords(StudentTotalCount).Username = Mid(strOneLine, 1, 7) '1 to 7
ArrStudentRecords(StudentTotalCount).Password = Mid(strOneLine, 15, 25) '15 to 25
End If
Wend
Close #1
Exit Sub

OopsLabel:
MsgBox "the following error has occured:" & vbNewLine & "Error #" & Err.Number & vbNewLine & Err.Description, vbCritical, "Open Error" 'error trapping
End Sub ' readfile

