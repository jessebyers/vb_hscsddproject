VERSION 5.00
Begin VB.Form frmBusNumbers 
   BackColor       =   &H8000000D&
   Caption         =   "Bus Numbers/Routes"
   ClientHeight    =   5340
   ClientLeft      =   4260
   ClientTop       =   4485
   ClientWidth     =   10230
   Icon            =   "frmBusNumbers.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5340
   ScaleWidth      =   10230
   Begin VB.CommandButton cmdBack 
      Caption         =   "Back"
      Height          =   615
      Left            =   8760
      TabIndex        =   3
      Top             =   4560
      Width           =   1335
   End
   Begin VB.CommandButton cmdFind 
      Caption         =   "Find Bus Route"
      Height          =   615
      Left            =   4680
      TabIndex        =   2
      Top             =   2640
      Width           =   1815
   End
   Begin VB.TextBox txtBusRoute 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   510
      Left            =   960
      MaxLength       =   3
      TabIndex        =   1
      Top             =   2520
      Width           =   1815
   End
   Begin VB.Image Image3 
      Height          =   825
      Left            =   7440
      Picture         =   "frmBusNumbers.frx":1BFD9
      Stretch         =   -1  'True
      Top             =   2760
      Width           =   2205
   End
   Begin VB.Image Image2 
      Height          =   810
      Left            =   6960
      Picture         =   "frmBusNumbers.frx":1CCD6
      Stretch         =   -1  'True
      Top             =   1680
      Width           =   3135
   End
   Begin VB.Label Label3 
      BackColor       =   &H8000000D&
      Caption         =   "Location:"
      BeginProperty Font 
         Name            =   "Microsoft Sans Serif"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      TabIndex        =   6
      Top             =   3720
      Width           =   1455
   End
   Begin VB.Label Label2 
      BackColor       =   &H8000000D&
      Caption         =   "Need to find out what bus you need to take?"
      BeginProperty Font 
         Name            =   "Microsoft Sans Serif"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   960
      TabIndex        =   5
      Top             =   360
      Width           =   7215
   End
   Begin VB.Label Label1 
      BackColor       =   &H8000000D&
      Caption         =   "Please type in the bus number:"
      BeginProperty Font 
         Name            =   "Microsoft Sans Serif"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      TabIndex        =   4
      Top             =   1680
      Width           =   5415
   End
   Begin VB.Image Image1 
      Height          =   1095
      Left            =   9000
      Picture         =   "frmBusNumbers.frx":2F408
      Stretch         =   -1  'True
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label lblBusNo 
      BackColor       =   &H8000000D&
      BeginProperty Font 
         Name            =   "Microsoft Sans Serif"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2160
      TabIndex        =   0
      Top             =   3720
      Width           =   5175
   End
End
Attribute VB_Name = "frmBusNumbers"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdBack_Click()
frmMainMenu.Show
Unload Me
End Sub

Private Sub cmdFind_Click() 'type the bus number and it shows you where its travelling too
Dim BusRoute As String


BusRoute = txtBusRoute.Text

Select Case BusRoute

Case 111 'bus number
lblBusNo.Caption = "Thornton" 'suburb
Case 222
lblBusNo.Caption = "Kurri"
Case 333
lblBusNo.Caption = "East Maitland"
Case 444
lblBusNo.Caption = "Maitland"
Case 555
lblBusNo.Caption = "Rathluba"
Case Else
lblBusNo.Caption = "Sorry, unknown bus route"
End Select
End Sub

Private Sub Form_Load()
PerCount = 0
End Sub


