VERSION 5.00
Begin VB.Form frmStudentCard 
   BackColor       =   &H8000000D&
   Caption         =   "Student Card"
   ClientHeight    =   5340
   ClientLeft      =   4260
   ClientTop       =   4485
   ClientWidth     =   10230
   Icon            =   "frmSchoolinghours.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5340
   ScaleWidth      =   10230
   Begin VB.CommandButton cmdBack 
      Caption         =   "Back"
      Height          =   615
      Left            =   8760
      TabIndex        =   0
      Top             =   4560
      Width           =   1335
   End
   Begin VB.Label Label1 
      BackColor       =   &H8000000D&
      Caption         =   "Your Student Card"
      BeginProperty Font 
         Name            =   "Corbel"
         Size            =   21.75
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   3120
      TabIndex        =   5
      Top             =   120
      Width           =   3735
   End
   Begin VB.Image Image1 
      Height          =   2250
      Left            =   6000
      Picture         =   "frmSchoolinghours.frx":1BFD9
      Top             =   1560
      Width           =   2250
   End
   Begin VB.Label LabelCentreNo 
      BackColor       =   &H8000000D&
      Caption         =   "Centre No."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   600
      TabIndex        =   4
      Top             =   2880
      Width           =   1575
   End
   Begin VB.Label LabelBOS 
      BackColor       =   &H8000000D&
      Caption         =   "BOS No."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   600
      TabIndex        =   3
      Top             =   1800
      Width           =   1335
   End
   Begin VB.Label lblCentreNo 
      BackColor       =   &H8000000D&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   2280
      TabIndex        =   2
      Top             =   2880
      Width           =   2895
   End
   Begin VB.Label lblBOSNo 
      BackColor       =   &H8000000D&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   2040
      TabIndex        =   1
      Top             =   1800
      Width           =   2895
   End
   Begin VB.Image Image2 
      Height          =   1095
      Left            =   9000
      Picture         =   "frmSchoolinghours.frx":1DC84
      Stretch         =   -1  'True
      Top             =   120
      Width           =   1095
   End
End
Attribute VB_Name = "frmStudentCard"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdBack_Click()
frmMainMenu.Show
Unload Me
End Sub
Private Function Add(ByVal x As Long, ByVal y As Long, ByVal z As Long) As Long
Dim Res As Long
Res = (x + y) * z 'formula for function
Add = Res
End Function
Private Function Add1(ByVal m As Long, ByVal n As Long) As Long
Dim Res1 As Long
Res1 = m * n
Add1 = Res1
End Function

Private Sub Form_Load()
PerCount = 0
Dim a As Long
Dim b As Long
Dim c As Long
a = 1062828 'variables for function
b = 10000000
c = 2
d = Add(a, b, c)
lblBOSNo = d

Dim e As Long
Dim f As Long
e = 387
f = 2
g = Add1(e, f)
lblCentreNo = g
End Sub

