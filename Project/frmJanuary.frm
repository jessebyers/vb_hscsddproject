VERSION 5.00
Begin VB.Form frmCalender 
   BackColor       =   &H8000000D&
   Caption         =   "2011 Calender"
   ClientHeight    =   5355
   ClientLeft      =   4335
   ClientTop       =   4560
   ClientWidth     =   10185
   Icon            =   "frmJanuary.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5355
   ScaleWidth      =   10185
   Begin VB.CommandButton cmdBack 
      Caption         =   "Back"
      Height          =   615
      Left            =   8640
      TabIndex        =   3
      Top             =   4560
      Width           =   1335
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Add Note"
      Height          =   615
      Left            =   1680
      TabIndex        =   1
      Top             =   4560
      Width           =   1335
   End
   Begin VB.TextBox txtNote 
      Height          =   1335
      Left            =   360
      MousePointer    =   3  'I-Beam
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   3000
      Width           =   3855
   End
   Begin VB.Label Label1 
      BackColor       =   &H8000000D&
      Caption         =   "Calender Reminders?"
      BeginProperty Font 
         Name            =   "Consolas"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   4560
      TabIndex        =   4
      Top             =   240
      Width           =   4095
   End
   Begin VB.Image imgDecember 
      BorderStyle     =   1  'Fixed Single
      Height          =   2535
      Left            =   360
      Picture         =   "frmJanuary.frx":1BFD9
      Stretch         =   -1  'True
      Top             =   240
      Width           =   3855
   End
   Begin VB.Image imgNovember 
      BorderStyle     =   1  'Fixed Single
      Height          =   2535
      Left            =   360
      Picture         =   "frmJanuary.frx":1FE75
      Stretch         =   -1  'True
      Top             =   240
      Width           =   3855
   End
   Begin VB.Image imgOctober 
      BorderStyle     =   1  'Fixed Single
      Height          =   2535
      Left            =   360
      Picture         =   "frmJanuary.frx":23E50
      Stretch         =   -1  'True
      Top             =   240
      Width           =   3855
   End
   Begin VB.Image imgSeptember 
      BorderStyle     =   1  'Fixed Single
      Height          =   2535
      Left            =   360
      Picture         =   "frmJanuary.frx":27E67
      Stretch         =   -1  'True
      Top             =   240
      Width           =   3855
   End
   Begin VB.Image imgAugust 
      BorderStyle     =   1  'Fixed Single
      Height          =   2535
      Left            =   360
      Picture         =   "frmJanuary.frx":2BCAE
      Stretch         =   -1  'True
      Top             =   240
      Width           =   3855
   End
   Begin VB.Image imgJuly 
      BorderStyle     =   1  'Fixed Single
      Height          =   2535
      Left            =   360
      Picture         =   "frmJanuary.frx":2FB2C
      Stretch         =   -1  'True
      Top             =   240
      Width           =   3855
   End
   Begin VB.Image imgJune 
      BorderStyle     =   1  'Fixed Single
      Height          =   2535
      Left            =   360
      Picture         =   "frmJanuary.frx":33A4F
      Stretch         =   -1  'True
      Top             =   240
      Width           =   3855
   End
   Begin VB.Image imgMay 
      BorderStyle     =   1  'Fixed Single
      Height          =   2535
      Left            =   360
      Picture         =   "frmJanuary.frx":37635
      Stretch         =   -1  'True
      Top             =   240
      Width           =   3855
   End
   Begin VB.Image imgApril 
      BorderStyle     =   1  'Fixed Single
      Height          =   2535
      Left            =   360
      Picture         =   "frmJanuary.frx":3B34F
      Stretch         =   -1  'True
      Top             =   240
      Width           =   3855
   End
   Begin VB.Image imgMarch 
      BorderStyle     =   1  'Fixed Single
      Height          =   2535
      Left            =   360
      Picture         =   "frmJanuary.frx":3F220
      Stretch         =   -1  'True
      Top             =   240
      Width           =   3855
   End
   Begin VB.Image imgFebruary 
      BorderStyle     =   1  'Fixed Single
      Height          =   2535
      Left            =   360
      Picture         =   "frmJanuary.frx":4317C
      Stretch         =   -1  'True
      Top             =   240
      Visible         =   0   'False
      Width           =   3855
   End
   Begin VB.Image Image2 
      Height          =   1095
      Left            =   9000
      Picture         =   "frmJanuary.frx":52BFD
      Stretch         =   -1  'True
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label lblNoteShow 
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3495
      Left            =   4560
      TabIndex        =   2
      Top             =   840
      Width           =   3855
   End
   Begin VB.Image imgJanuary 
      BorderStyle     =   1  'Fixed Single
      Height          =   2505
      Left            =   360
      Picture         =   "frmJanuary.frx":548A8
      Stretch         =   -1  'True
      Top             =   240
      Visible         =   0   'False
      Width           =   3825
   End
End
Attribute VB_Name = "frmCalender"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public StrTextFile As String

Private Sub cmdBack_Click()
frmMainMenu.Show
Unload Me
End Sub


Private Sub cmdSave_Click()
'the code is repeated when the form loads. This code shows and updates the label automatically with just 1 click of the button
Open StrTextFile For Append As #1
Print #1, txtNote.Text 'prints to the text file
Close #1
lblNoteShow.Caption = ""

Dim StrContents As String
Open StrTextFile For Input As 1 'opens the text file
Do While Not EOF(1)
Line Input #1, StrContents
lblNoteShow.Caption = lblNoteShow.Caption & StrContents & Chr(13) & Chr(10) 'what it will be typed as in the text file
Loop
Close #1
txtNote.Text = "" ''after saving, to avoid error or duplication in text, it will clear the text box
End Sub


Private Sub Form_Load()
PerCount = 0
Dim strMonth As String

Do
strMonth = InputBox("Please enter a Month") 'post test loop, the month must be entered

Loop Until strMonth = "January" Or strMonth = "February" Or strMonth = "March" Or strMonth = "April" Or strMonth = "May" Or strMonth = "June" Or strMonth = "July" Or strMonth = "August" Or strMonth = "September" Or strMonth = "October" Or strMonth = "November" Or strMonth = "December" Or strMonth = "january" Or strMonth = "february" Or strMonth = "march" Or strMonth = "april" Or strMonth = "may" Or strMonth = "june" Or strMonth = "july" Or strMonth = "august" Or strMonth = "september" Or strMonth = "october" Or strMonth = "november" Or strMonth = "december"

'when a month is entered correctly, the calender image for that month will appear while the rest of the images become non visible

If strMonth = "January" Or strMonth = "january" Then
imgJanuary.Visible = True
imgFebruary.Visible = False
imgMarch.Visible = False
imgApril.Visible = False
imgMay.Visible = False
imgJune.Visible = False
imgJuly.Visible = False
imgAugust.Visible = False
imgSeptember.Visible = False
imgOctober.Visible = False
imgNovember.Visible = False
imgDecember.Visible = False
End If

If strMonth = "February" Or strMonth = "february" Then
imgJanuary.Visible = False
imgFebruary.Visible = True
imgMarch.Visible = False
imgApril.Visible = False
imgMay.Visible = False
imgJune.Visible = False
imgJuly.Visible = False
imgAugust.Visible = False
imgSeptember.Visible = False
imgOctober.Visible = False
imgNovember.Visible = False
imgDecember.Visible = False
End If
If strMonth = "March" Or strMonth = "march" Then
imgJanuary.Visible = False
imgFebruary.Visible = False
imgMarch.Visible = True
imgApril.Visible = False
imgMay.Visible = False
imgJune.Visible = False
imgJuly.Visible = False
imgAugust.Visible = False
imgSeptember.Visible = False
imgOctober.Visible = False
imgNovember.Visible = False
imgDecember.Visible = False
End If

If strMonth = "April" Or strMonth = "april" Then
imgJanuary.Visible = False
imgFebruary.Visible = False
imgMarch.Visible = False
imgApril.Visible = True
imgMay.Visible = False
imgJune.Visible = False
imgJuly.Visible = False
imgAugust.Visible = False
imgSeptember.Visible = False
imgOctober.Visible = False
imgNovember.Visible = False
imgDecember.Visible = False
End If

If strMonth = "May" Or strMonth = "may" Then
imgJanuary.Visible = False
imgFebruary.Visible = False
imgMarch.Visible = False
imgApril.Visible = False
imgMay.Visible = True
imgJune.Visible = False
imgJuly.Visible = False
imgAugust.Visible = False
imgSeptember.Visible = False
imgOctober.Visible = False
imgNovember.Visible = False
imgDecember.Visible = False
End If

If strMonth = "June" Or strMonth = "june" Then
imgJanuary.Visible = False
imgFebruary.Visible = False
imgMarch.Visible = False
imgApril.Visible = False
imgMay.Visible = False
imgJune.Visible = True
imgJuly.Visible = False
imgAugust.Visible = False
imgSeptember.Visible = False
imgOctober.Visible = False
imgNovember.Visible = False
imgDecember.Visible = False
End If

If strMonth = "July" Or strMonth = "july" Then
imgJanuary.Visible = False
imgFebruary.Visible = False
imgMarch.Visible = False
imgApril.Visible = False
imgMay.Visible = False
imgJune.Visible = False
imgJuly.Visible = True
imgAugust.Visible = False
imgSeptember.Visible = False
imgOctober.Visible = False
imgNovember.Visible = False
imgDecember.Visible = False
End If

If strMonth = "August" Or strMonth = "august" Then
imgJanuary.Visible = False
imgFebruary.Visible = False
imgMarch.Visible = False
imgApril.Visible = False
imgMay.Visible = False
imgJune.Visible = False
imgJuly.Visible = False
imgAugust.Visible = True
imgSeptember.Visible = False
imgOctober.Visible = False
imgNovember.Visible = False
imgDecember.Visible = False
End If

If strMonth = "September" Or strMonth = "september" Then
imgJanuary.Visible = False
imgFebruary.Visible = False
imgMarch.Visible = False
imgApril.Visible = False
imgMay.Visible = False
imgJune.Visible = False
imgJuly.Visible = False
imgAugust.Visible = False
imgSeptember.Visible = True
imgOctober.Visible = False
imgNovember.Visible = False
imgDecember.Visible = False
End If

If strMonth = "October" Or strMonth = "october" Then
imgJanuary.Visible = False
imgFebruary.Visible = False
imgMarch.Visible = False
imgApril.Visible = False
imgMay.Visible = False
imgJune.Visible = False
imgJuly.Visible = False
imgAugust.Visible = False
imgSeptember.Visible = False
imgOctober.Visible = True
imgNovember.Visible = False
imgDecember.Visible = False
End If

If strMonth = "November" Or strMonth = "november" Then
imgJanuary.Visible = False
imgFebruary.Visible = False
imgMarch.Visible = False
imgApril.Visible = False
imgMay.Visible = False
imgJune.Visible = False
imgJuly.Visible = False
imgAugust.Visible = False
imgSeptember.Visible = False
imgOctober.Visible = False
imgNovember.Visible = True
imgDecember.Visible = False
End If

If strMonth = "December" Or strMonth = "december" Then
imgJanuary.Visible = False
imgFebruary.Visible = False
imgMarch.Visible = False
imgApril.Visible = False
imgMay.Visible = False
imgJune.Visible = False
imgJuly.Visible = False
imgAugust.Visible = False
imgSeptember.Visible = False
imgOctober.Visible = False
imgNovember.Visible = False
imgDecember.Visible = True
End If



'This code is executed when the form loads, so that the text can automatically be shown without the use of a button
StrTextFile = App.Path & "\TextFiles\txtCalender.txt"

Dim StrContents As String
Open StrTextFile For Input As 1
Do While Not EOF(1)
Line Input #1, StrContents 'writes to file
lblNoteShow.Caption = lblNoteShow.Caption & StrContents & Chr(13) & Chr(10)
Loop
Close #1
txtNote.Text = "" 'clears the text box when save is pressed. Avoids duplication of text
End Sub


Private Sub Image2_Click()
frmMainMenu.Show
Unload Me
End Sub


