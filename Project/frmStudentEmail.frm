VERSION 5.00
Begin VB.Form frmSchoolEmail 
   BackColor       =   &H8000000D&
   Caption         =   "School Email"
   ClientHeight    =   5325
   ClientLeft      =   4260
   ClientTop       =   4485
   ClientWidth     =   10260
   ForeColor       =   &H00000000&
   Icon            =   "frmStudentEmail.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5325
   ScaleWidth      =   10260
   Begin VB.CommandButton cmdSearch 
      Caption         =   "Search"
      Height          =   495
      Left            =   720
      TabIndex        =   6
      Top             =   2520
      Width           =   1695
   End
   Begin VB.TextBox txtSearch 
      Height          =   495
      Left            =   480
      TabIndex        =   5
      Top             =   1800
      Width           =   2175
   End
   Begin VB.CommandButton cmdSort 
      Caption         =   "Sort Alphabetically"
      Height          =   495
      Left            =   3360
      TabIndex        =   2
      Top             =   4560
      Width           =   2295
   End
   Begin VB.ListBox lstAddress 
      Height          =   3375
      Left            =   3000
      TabIndex        =   1
      Top             =   960
      Width           =   3015
   End
   Begin VB.CommandButton cmdBack 
      Caption         =   "Back"
      Height          =   615
      Left            =   8760
      TabIndex        =   0
      Top             =   4560
      Width           =   1335
   End
   Begin VB.Image Image1 
      Height          =   2175
      Left            =   6480
      Picture         =   "frmStudentEmail.frx":1BFD9
      Stretch         =   -1  'True
      Top             =   1560
      Width           =   2415
   End
   Begin VB.Label lblMaitenance 
      BackColor       =   &H8000000D&
      Caption         =   "Under Development!!!"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   735
      Left            =   360
      TabIndex        =   4
      Top             =   480
      Width           =   2415
   End
   Begin VB.Label Label1 
      BackColor       =   &H8000000D&
      Caption         =   "School Email Addresses"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   20.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3120
      TabIndex        =   3
      Top             =   120
      Width           =   3015
   End
   Begin VB.Image Image2 
      Height          =   1095
      Left            =   9000
      Picture         =   "frmStudentEmail.frx":26113
      Stretch         =   -1  'True
      Top             =   120
      Width           =   1095
   End
End
Attribute VB_Name = "frmSchoolEmail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Code for this module is not completed. Will be further developed in the future

Private Sub cmdBack_Click()
frmMainMenu.Show
Unload Me
End Sub

Private Sub Form_Load()
PerCount = 0
Call LoadEmailAddress ' calls the sub in mdlEmail
End Sub

Private Sub cmdSort_Click()

Dim intx As Integer
For intx = 0 To 1000
For intCounter = 0 To 998
intCounter2 = intCounter + 1
If arrNumber(intCounter) > arrNumber(intCounter2) Then
intSpare = arrNumber(intCounter)
arrNumber(intCounter) = arrNumber(intCounter2)
arrNumber(intCounter2) = intSpare
End If

Next intCounter

Next intx

lstAddress.Clear 'clears listbox for sorting

For y = 0 To 999
lstAddress.AddItem arrNumber(y) 'sorted numbers into listbox
Next y

End Sub

