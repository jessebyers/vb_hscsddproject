VERSION 5.00
Begin VB.Form frmMainMenu 
   BackColor       =   &H8000000D&
   Caption         =   "eDiary"
   ClientHeight    =   5325
   ClientLeft      =   4275
   ClientTop       =   4500
   ClientWidth     =   10200
   ForeColor       =   &H00000000&
   Icon            =   "frmMainMenu.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5325
   ScaleWidth      =   10200
   Begin VB.CommandButton cmdLogOut 
      Caption         =   "Log Out"
      Height          =   615
      Left            =   8640
      TabIndex        =   1
      Top             =   4560
      Width           =   1335
   End
   Begin VB.Image Image8 
      BorderStyle     =   1  'Fixed Single
      Height          =   975
      Left            =   4800
      MousePointer    =   2  'Cross
      Picture         =   "frmMainMenu.frx":1BFD9
      Stretch         =   -1  'True
      Top             =   120
      Width           =   1095
   End
   Begin VB.Image Image7 
      BorderStyle     =   1  'Fixed Single
      Height          =   975
      Left            =   3240
      MousePointer    =   2  'Cross
      Picture         =   "frmMainMenu.frx":1EDF7
      Stretch         =   -1  'True
      Top             =   120
      Width           =   1095
   End
   Begin VB.Image Image6 
      BorderStyle     =   1  'Fixed Single
      Height          =   975
      Left            =   1680
      MousePointer    =   2  'Cross
      Picture         =   "frmMainMenu.frx":1FE00
      Stretch         =   -1  'True
      Top             =   120
      Width           =   1095
   End
   Begin VB.Image Image5 
      BorderStyle     =   1  'Fixed Single
      Height          =   975
      Left            =   120
      MousePointer    =   2  'Cross
      Picture         =   "frmMainMenu.frx":204E5
      Stretch         =   -1  'True
      Top             =   120
      Width           =   1095
   End
   Begin VB.Image Image2 
      BorderStyle     =   1  'Fixed Single
      Height          =   975
      Left            =   4800
      MousePointer    =   2  'Cross
      Picture         =   "frmMainMenu.frx":2162B
      Stretch         =   -1  'True
      Top             =   4200
      Width           =   1095
   End
   Begin VB.Image imgBOS 
      BorderStyle     =   1  'Fixed Single
      Height          =   615
      Left            =   8280
      MousePointer    =   2  'Cross
      Picture         =   "frmMainMenu.frx":21F8B
      Stretch         =   -1  'True
      Top             =   2760
      Width           =   1815
   End
   Begin VB.Image imgSchoolPortal 
      BorderStyle     =   1  'Fixed Single
      Height          =   615
      Left            =   8280
      MousePointer    =   2  'Cross
      Picture         =   "frmMainMenu.frx":237EB
      Stretch         =   -1  'True
      Top             =   2040
      Width           =   1815
   End
   Begin VB.Image imgGoogle 
      BorderStyle     =   1  'Fixed Single
      Height          =   570
      Left            =   8280
      MousePointer    =   2  'Cross
      Picture         =   "frmMainMenu.frx":240D7
      Stretch         =   -1  'True
      Top             =   3480
      Width           =   1785
   End
   Begin VB.Image Image1 
      BorderStyle     =   1  'Fixed Single
      Height          =   975
      Left            =   3240
      MousePointer    =   2  'Cross
      Picture         =   "frmMainMenu.frx":2622C
      Stretch         =   -1  'True
      Top             =   4200
      Width           =   1095
   End
   Begin VB.Image Image3 
      BorderStyle     =   1  'Fixed Single
      Height          =   975
      Left            =   1680
      MousePointer    =   2  'Cross
      Picture         =   "frmMainMenu.frx":291D0
      Stretch         =   -1  'True
      Top             =   4200
      Width           =   1095
   End
   Begin VB.Image Image4 
      BorderStyle     =   1  'Fixed Single
      Height          =   975
      Left            =   120
      MousePointer    =   2  'Cross
      Picture         =   "frmMainMenu.frx":34CFC
      Stretch         =   -1  'True
      Top             =   4200
      Width           =   1095
   End
   Begin VB.Label lblDate 
      BackColor       =   &H8000000D&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   360
      TabIndex        =   2
      Top             =   2760
      Width           =   2535
   End
   Begin VB.Image imgLogo 
      Height          =   1650
      Left            =   8280
      MousePointer    =   2  'Cross
      Picture         =   "frmMainMenu.frx":35A70
      Stretch         =   -1  'True
      Top             =   120
      Width           =   1770
   End
   Begin VB.Label lbleDiary 
      BackColor       =   &H8000000D&
      Caption         =   "eDiary"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   27.75
         Charset         =   0
         Weight          =   900
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   360
      TabIndex        =   0
      Top             =   1680
      Width           =   2175
   End
End
Attribute VB_Name = "frmMainMenu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'for the hyperlinks
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, _
ByVal lpOperation As String, _
ByVal lpFile As String, _
ByVal lpParameters As String, _
ByVal lpDirectory As String, _
ByVal nShowCmd As Long) As Long

Private Sub cmdLogOut_Click() 'back to login
frmLogin.Show
Unload Me
End Sub

Private Sub Form_Load()
lblDate.Caption = Format(Now(), "dddd, dd mmmm yyyy ") 'current systems date
ReadTimeTableFile

End Sub


Private Sub Image1_Click() 'shows notes form
frmNotes.Show
Unload Me
End Sub

Private Sub Image2_Click() 'shows student card form
frmStudentCard.Show
Unload Me
End Sub

Private Sub Image5_Click() 'shows average percentage form
frmAvgPercen.Show
Unload Me
End Sub

Private Sub Image6_Click() 'shows bus numbers form
frmBusNumbers.Show
Unload Me
End Sub



Private Sub Image7_Click() 'shows subjects form
frmSubjects.Show
Unload Me
End Sub

Private Sub Image8_Click() 'shows school email form
frmSchoolEmail.Show
Unload Me
End Sub

Private Sub imgGoogle_Click() 'hyperlink for google
ShellExecute frmMainMenu.hwnd, "open", "http://www.google.com.au", "", "", SW_SHOW Or SW_NORMAL
End Sub


Private Sub Image3_Click() 'shows calender form
frmCalender.Show
Unload Me
End Sub

Private Sub Image4_Click() 'shows timetable form
frmTimetable.Show
Unload Me
End Sub

Private Sub imgBOS_Click() 'hyperlink for board of studies
ShellExecute frmMainMenu.hwnd, "open", "http://www.boardofstudies.nsw.edu.au/", "", "", SW_SHOW Or SW_NORMAL
End Sub

Private Sub imgLogo_Click() 'shows school info form
frmSchoolInfo.Show
Unload Me
End Sub

Private Sub imgSchoolPortal_Click() 'hyperlink to the schools portal
ShellExecute frmMainMenu.hwnd, "open", "http://intra.maitsm.mn.catholic.edu.au/", "", "", SW_SHOW Or SW_NORMAL
End Sub


