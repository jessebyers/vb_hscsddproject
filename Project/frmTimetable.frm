VERSION 5.00
Begin VB.Form frmTimetable 
   BackColor       =   &H8000000D&
   Caption         =   "TimeTable -Tuesday"
   ClientHeight    =   5325
   ClientLeft      =   4335
   ClientTop       =   4395
   ClientWidth     =   10200
   Icon            =   "frmTimetable.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5325
   ScaleWidth      =   10200
   Begin VB.CommandButton cmdBack 
      Caption         =   "Back"
      Height          =   615
      Left            =   8640
      TabIndex        =   8
      Top             =   4560
      Width           =   1335
   End
   Begin VB.ListBox lstPeriods 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3030
      Left            =   360
      TabIndex        =   0
      Top             =   1320
      Width           =   2535
   End
   Begin VB.Image Image2 
      Height          =   2295
      Left            =   7440
      Picture         =   "frmTimetable.frx":1BFD9
      Stretch         =   -1  'True
      Top             =   1440
      Width           =   2535
   End
   Begin VB.Image Image1 
      Height          =   1095
      Left            =   9000
      Picture         =   "frmTimetable.frx":6959B
      Stretch         =   -1  'True
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label LabelTimeTable 
      BackColor       =   &H8000000D&
      Caption         =   "Timetable"
      BeginProperty Font 
         Name            =   "Microsoft Sans Serif"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   7
      Top             =   240
      Width           =   3375
   End
   Begin VB.Label lblClassRoom 
      BackColor       =   &H8000000D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5400
      TabIndex        =   6
      Top             =   2640
      Width           =   1335
   End
   Begin VB.Label LabelClass 
      BackColor       =   &H8000000D&
      Caption         =   "Class Room:"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3000
      TabIndex        =   5
      Top             =   2640
      Width           =   2175
   End
   Begin VB.Label lblTime 
      BackColor       =   &H8000000D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4200
      TabIndex        =   4
      Top             =   3960
      Width           =   3135
   End
   Begin VB.Label LabelTime 
      BackColor       =   &H8000000D&
      Caption         =   "Time:"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3000
      TabIndex        =   3
      Top             =   3960
      Width           =   975
   End
   Begin VB.Label lblSubject 
      BackColor       =   &H8000000D&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4800
      TabIndex        =   2
      Top             =   1560
      Width           =   1935
   End
   Begin VB.Label LabelSubject 
      BackColor       =   &H8000000D&
      Caption         =   "Subject:"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3000
      TabIndex        =   1
      Top             =   1560
      Width           =   1575
   End
End
Attribute VB_Name = "frmTimetable"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdBack_Click()
lstPeriods.Clear
PerCount = 0
frmMainMenu.Show
Unload Me
End Sub

Private Sub Form_Load()
Dim per As Integer

'ReadTimeTableFile
lstPeriods.Clear

For per = 1 To PerCount
lstPeriods.AddItem Trim(PerArray(per).Period)
'lstPeriods.ItemData(lstPeriods.NewIndex) = per
Next per

End Sub




Private Sub lstPeriods_Click()
Dim Index As Integer

Index = lstPeriods.ListIndex + 1

lblSubject = PerArray(Index).Subject
lblTime = PerArray(Index).Time
lblClassRoom = PerArray(Index).ClassRoom
End Sub
