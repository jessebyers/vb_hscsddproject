VERSION 5.00
Begin VB.Form frmRegister 
   BackColor       =   &H8000000D&
   Caption         =   "Register"
   ClientHeight    =   5325
   ClientLeft      =   4290
   ClientTop       =   4485
   ClientWidth     =   10230
   Icon            =   "frmRegister.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5325
   ScaleWidth      =   10230
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   615
      Left            =   8640
      TabIndex        =   6
      Top             =   4560
      Width           =   1335
   End
   Begin VB.TextBox txtRegPass 
      Height          =   375
      Left            =   3360
      MaxLength       =   10
      TabIndex        =   2
      Top             =   1920
      Width           =   3855
   End
   Begin VB.CommandButton cmdRegister 
      Caption         =   "Register"
      Height          =   615
      Left            =   4320
      TabIndex        =   1
      Top             =   2520
      Width           =   1335
   End
   Begin VB.TextBox txtRegUser 
      Height          =   375
      Left            =   3360
      MaxLength       =   7
      TabIndex        =   0
      Top             =   1320
      Width           =   3855
   End
   Begin VB.Image Image3 
      Height          =   1335
      Left            =   8040
      Picture         =   "frmRegister.frx":1BFD9
      Stretch         =   -1  'True
      Top             =   1680
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Image Image1 
      Height          =   1935
      Left            =   7680
      Picture         =   "frmRegister.frx":1CF53
      Stretch         =   -1  'True
      Top             =   1440
      Width           =   2055
   End
   Begin VB.Label Label1 
      BackColor       =   &H8000000D&
      Caption         =   "Not Registered?"
      BeginProperty Font 
         Name            =   "Calibri"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   3480
      TabIndex        =   8
      Top             =   240
      Width           =   3375
   End
   Begin VB.Label lblInfo 
      BackColor       =   &H8000000D&
      Caption         =   "Note: Username Must Be 1 - 7 Characters Long and Password Must Be 1 - 10 Characters Long."
      BeginProperty Font 
         Name            =   "Calibri"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1560
      TabIndex        =   7
      Top             =   3600
      Width           =   7215
   End
   Begin VB.Image Image2 
      Height          =   1095
      Left            =   9000
      Picture         =   "frmRegister.frx":214B8
      Stretch         =   -1  'True
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label lblSuccess 
      BackColor       =   &H8000000D&
      Caption         =   "Your New Account has been Registered"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   15.75
         Charset         =   0
         Weight          =   900
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   1560
      TabIndex        =   5
      Top             =   3600
      Visible         =   0   'False
      Width           =   7095
   End
   Begin VB.Label LabelPass 
      BackColor       =   &H8000000D&
      Caption         =   "New Password:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1320
      TabIndex        =   4
      Top             =   1920
      Width           =   1935
   End
   Begin VB.Label LabelUser 
      BackColor       =   &H8000000D&
      Caption         =   "New Username:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1320
      TabIndex        =   3
      Top             =   1320
      Width           =   1935
   End
End
Attribute VB_Name = "frmRegister"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public StrTextFile As String

Private Sub cmdCancel_Click()
frmLogin.Show
Unload Me
End Sub

Private Sub cmdRegister_Click()
Open StrTextFile For Append As #1
Print #1, txtRegUser.Text, txtRegPass.Text 'text code that prints to students text file

Close 1
Image3.Visible = True
lblSuccess.Visible = True
lblInfo.Visible = False
cmdCancel.Caption = "Back"
End Sub

Private Sub Form_Load()
PerCount = 0 'to avoid duplication of the lstbox in frmTimetable
cmdCancel.Caption = "Cancel"
StrTextFile = App.Path & "\TextFiles\student.txt" 'path of file
End Sub

