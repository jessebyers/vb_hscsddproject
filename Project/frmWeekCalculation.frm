VERSION 5.00
Begin VB.Form frmAvgPercen 
   BackColor       =   &H8000000D&
   Caption         =   "Average Percentage Mark"
   ClientHeight    =   5340
   ClientLeft      =   4260
   ClientTop       =   4485
   ClientWidth     =   10305
   Icon            =   "frmWeekCalculation.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5340
   ScaleWidth      =   10305
   Begin VB.CommandButton cmdCalc 
      Caption         =   "Calculate Average Percentage Mark"
      Height          =   615
      Left            =   4080
      TabIndex        =   1
      Top             =   4560
      Width           =   1815
   End
   Begin VB.CommandButton cmdBack 
      Caption         =   "Back"
      Height          =   615
      Left            =   8760
      TabIndex        =   0
      Top             =   4560
      Width           =   1335
   End
   Begin VB.Image Image1 
      Height          =   2055
      Left            =   720
      Picture         =   "frmWeekCalculation.frx":1BFD9
      Stretch         =   -1  'True
      Top             =   3000
      Width           =   2415
   End
   Begin VB.Image Image2 
      Height          =   1095
      Left            =   9000
      Picture         =   "frmWeekCalculation.frx":20B42
      Stretch         =   -1  'True
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label LabelAvgPercen 
      BackColor       =   &H8000000D&
      Caption         =   "Whats Your Average Assessment Task Mark?"
      BeginProperty Font 
         Name            =   "Calibri"
         Size            =   21.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   600
      TabIndex        =   4
      Top             =   240
      Width           =   8295
   End
   Begin VB.Label lblPercentage 
      BackColor       =   &H8000000D&
      BeginProperty Font 
         Name            =   "Calibri"
         Size            =   20.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   3
      Top             =   2160
      Width           =   6255
   End
   Begin VB.Label lblAssess 
      BackColor       =   &H8000000D&
      BeginProperty Font 
         Name            =   "Calibri"
         Size            =   20.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   2
      Top             =   1440
      Width           =   8175
   End
End
Attribute VB_Name = "frmAvgPercen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdBack_Click()
frmMainMenu.Show
Unload Me
End Sub

Private Sub cmdCalc_Click() ' works out what your average assessment task percentage is based on your past assessment tasks

Dim intAssessmentTasks As Integer
Dim intMark As Integer
Dim intMarkCtr As Integer
Dim intMarkTotal As Integer

StartAgain: 'traps overflow errors
intAssessmentTasks = Val(InputBox("Enter the number of tasks you have recieved")) 'determines how many times the loop will happen

If intAssessmentTasks > 20 Then
MsgBox "Too long, needs to be a value between 1 - 20", vbExclamation, "Note"
GoTo StartAgain
End If

Try: 'traps overflow errors
For intMarkCtr = 1 To intAssessmentTasks 'counted loop depends on what the user puts in
intMark = Val(InputBox("Enter your score as a percentage in each assessment task you have completed"))

If intMark > 100 Then
MsgBox "Needs to be a percentage, cannot be over 100", vbExclamation, "Note"
GoTo Try
End If

intMarkTotal = intMark + intMarkTotal 'the average
Next

lblAssess.Caption = "You have had " & intAssessmentTasks & " assessment tasks so far"
lblPercentage.Caption = "Your average percentage is " & intMarkTotal / intAssessmentTasks & "%" 'works out the average

End Sub


Private Sub Form_Load()
PerCount = 0
End Sub
