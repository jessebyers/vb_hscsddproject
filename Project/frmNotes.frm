VERSION 5.00
Begin VB.Form frmNotes 
   BackColor       =   &H8000000D&
   Caption         =   "Notes"
   ClientHeight    =   5340
   ClientLeft      =   4275
   ClientTop       =   4500
   ClientWidth     =   10290
   Icon            =   "frmNotes.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5340
   ScaleWidth      =   10290
   Begin VB.CommandButton cmdSave 
      Caption         =   "Save and Go Back"
      Height          =   615
      Left            =   8520
      TabIndex        =   1
      Top             =   4440
      Width           =   1335
   End
   Begin VB.TextBox txtNotesShow 
      BackColor       =   &H00FFFFFF&
      Height          =   3975
      Left            =   240
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   1080
      Width           =   5895
   End
   Begin VB.Image Image1 
      Height          =   2415
      Left            =   6840
      Picture         =   "frmNotes.frx":1BFD9
      Stretch         =   -1  'True
      Top             =   1560
      Width           =   2535
   End
   Begin VB.Label Label1 
      BackColor       =   &H8000000D&
      Caption         =   "Got anything to write down?"
      BeginProperty Font 
         Name            =   "Lucida Fax"
         Size            =   18
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   2
      Top             =   240
      Width           =   6015
   End
   Begin VB.Image Image2 
      Height          =   1095
      Left            =   9000
      Picture         =   "frmNotes.frx":207C6
      Stretch         =   -1  'True
      Top             =   120
      Width           =   1095
   End
End
Attribute VB_Name = "frmNotes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public StrNotesTextFile As String


Private Sub cmdSave_Click()
PerCount = 0
StrNotesTextFile = App.Path & "\TextFiles\txtNotes.txt"
Kill (StrNotesTextFile) 'deletes text file
Open StrNotesTextFile For Append As #1 'recreates the text file
Print #1, txtNotesShow.Text 'prints the text in the text box
Close #1

frmMainMenu.Show
Unload Me
End Sub

Private Sub Form_Load()
'opens text file and puts the text in the file in the text box
StrNotesTextFile = App.Path & "\TextFiles\txtNotes.txt" 'path of the file
Dim StrContents As String
Open StrNotesTextFile For Input As 1
Do While Not EOF(1)
Line Input #1, StrContents
txtNotesShow.Text = txtNotesShow.Text & StrContents & Chr(13) & Chr(10)
Loop
Close #1
End Sub
