VERSION 5.00
Begin VB.Form frmSubjects 
   BackColor       =   &H8000000D&
   Caption         =   "Subjects"
   ClientHeight    =   5325
   ClientLeft      =   4275
   ClientTop       =   4500
   ClientWidth     =   10200
   Icon            =   "frmSubjects.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5325
   ScaleWidth      =   10200
   Begin VB.PictureBox picSubjects 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3255
      Left            =   3240
      ScaleHeight     =   3195
      ScaleWidth      =   3915
      TabIndex        =   2
      Top             =   1080
      Width           =   3975
   End
   Begin VB.CommandButton cmdPrintArray 
      Caption         =   "Show Subjects"
      Height          =   495
      Left            =   3960
      TabIndex        =   1
      Top             =   4560
      Width           =   2655
   End
   Begin VB.CommandButton cmdBack 
      Caption         =   "Back"
      Height          =   615
      Left            =   8640
      TabIndex        =   0
      Top             =   4560
      Width           =   1335
   End
   Begin VB.Image Image1 
      Height          =   3615
      Left            =   480
      Picture         =   "frmSubjects.frx":1BFD9
      Stretch         =   -1  'True
      Top             =   840
      Width           =   1815
   End
   Begin VB.Label Label3 
      BackColor       =   &H8000000D&
      Caption         =   "2U"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2640
      TabIndex        =   5
      Top             =   1440
      Width           =   375
   End
   Begin VB.Label Label2 
      BackColor       =   &H8000000D&
      Caption         =   "1U"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2640
      TabIndex        =   4
      Top             =   2760
      Width           =   495
   End
   Begin VB.Label Label1 
      BackColor       =   &H8000000D&
      Caption         =   "What Are Your Subjects??"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   14.25
         Charset         =   0
         Weight          =   900
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   3120
      TabIndex        =   3
      Top             =   240
      Width           =   4215
   End
   Begin VB.Image Image2 
      Height          =   1095
      Left            =   9000
      Picture         =   "frmSubjects.frx":2DFFC
      Stretch         =   -1  'True
      Top             =   120
      Width           =   1095
   End
End
Attribute VB_Name = "frmSubjects"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdBack_Click()
frmMainMenu.Show
Unload Me
End Sub

Private Sub cmdPrintArray_Click()
Dim intinner As Integer
Dim intouter As Integer

'two unit subjects
arrSubjects(0, 0) = "Maths"
arrSubjects(0, 1) = "English"
arrSubjects(0, 2) = "IPT"
arrSubjects(0, 3) = "Science"
arrSubjects(0, 4) = "Software Design and Developement"
'1 unit subjects
arrSubjects(1, 0) = ""
arrSubjects(1, 1) = "English Turorial"
arrSubjects(1, 2) = "Maths Tutorial"
arrSubjects(1, 3) = "Religion"


For Outer = 0 To 1
For Inner = 0 To 4

picSubjects.Print arrSubjects(Outer, Inner)
Next Inner
Next Outer
cmdPrintArray.Enabled = False 'stops duplication of the array on the form
End Sub


Private Sub Form_Load()
PerCount = 0
End Sub

